import React from "react";
import { useLocation, useHistory } from "react-router";

function About() {
  const location = useLocation();
  const history = useHistory();
  console.log(location);

  const goBackHandler = () => {
      history.goBack();
  }
  return (
    <>
      <div>About</div>
      <div>Location = {location.pathname}</div>
      <div>From = {location.state.from}</div>
      <button onClick={goBackHandler}>Go Back</button>
    </>
  );
}

export default About;
