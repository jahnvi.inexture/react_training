import React from "react";
import { Route, Switch } from "react-router-dom";
import SignUp from "./sign-up/SignUp";
import SignIn from "./sign-in/SignIn";
import "./index.css";
import Dashboard from "./dashboard/Dashboard";

function App() {
  return (
    <>
      <div className="App">
        <Switch>
          <Route path="/" exact component={SignUp} />
          <Route path="/signin" exact component={SignIn} />
          <Route path="/dashboard" exact component={Dashboard} />
        </Switch>
      </div>
    </>
  );
}

export default App;
