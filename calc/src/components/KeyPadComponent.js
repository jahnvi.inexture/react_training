import React, { Component } from "react";
class KeyPadComponent extends Component {
  state = {
    randomNumber: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
  };
  copiedList = [];

  singleNumber;
  count;
  shuffle = (e) => {
    this.count = 10;
    this.state.randomNumber = [];
    this.props.onClick(e.target.name);

    while (this.count !== 0) {
      this.singleNumber = Math.floor(Math.random() * 10);
      if (!this.state.randomNumber.includes(this.singleNumber)) {
        console.log(this.singleNumber);
        this.state.randomNumber.push(this.singleNumber);
        this.count--;
      } else {
        console.log("REjected" + this.singleNumber);
      }
    }
    this.copiedList = [...this.state.randomNumber];
    // console.log(this.copiedList);
    this.setState({
      randomNumber: this.copiedList,
    });
  };
  render() {
    return (
      <div className="button">
        <button name="(" onClick={(e) => this.props.onClick(e.target.name)}>
          (
        </button>
        <button name="CE" onClick={(e) => this.props.onClick(e.target.name)}>
          CE
        </button>
        <button name=")" onClick={(e) => this.props.onClick(e.target.name)}>
          )
        </button>
        <button name="C" onClick={(e) => this.props.onClick(e.target.name)}>
          C
        </button>
        <br />

        <button
          name={this.state.randomNumber[0]}
          onClick={(e) => this.shuffle(e)}
        >
          {this.state.randomNumber[0]}
        </button>
        <button
          name={this.state.randomNumber[1]}
          onClick={(e) => this.shuffle(e)}
        >
          {this.state.randomNumber[1]}
        </button>
        <button
          name={this.state.randomNumber[2]}
          onClick={(e) => this.shuffle(e)}
        >
          {this.state.randomNumber[2]}
        </button>
        <button name="+" onClick={(e) => this.props.onClick(e.target.name)}>
          +
        </button>
        <br />

        <button
          name={this.state.randomNumber[3]}
          onClick={(e) => this.shuffle(e)}
        >
          {this.state.randomNumber[3]}
        </button>
        <button
          name={this.state.randomNumber[4]}
          onClick={(e) => this.shuffle(e)}
        >
          {this.state.randomNumber[4]}
        </button>
        <button
          name={this.state.randomNumber[5]}
          onClick={(e) => this.shuffle(e)}
        >
          {this.state.randomNumber[5]}
        </button>
        <button name="-" onClick={(e) => this.props.onClick(e.target.name)}>
          -
        </button>
        <br />

        <button
          name={this.state.randomNumber[6]}
          onClick={(e) => this.shuffle(e)}
        >
          {this.state.randomNumber[6]}
        </button>
        <button
          name={this.state.randomNumber[7]}
          onClick={(e) => this.shuffle(e)}
        >
          {this.state.randomNumber[7]}
        </button>
        <button
          name={this.state.randomNumber[8]}
          onClick={(e) => this.shuffle(e)}
        >
          {this.state.randomNumber[8]}
        </button>
        <button name="*" onClick={(e) => this.props.onClick(e.target.name)}>
          x
        </button>
        <br />

        <button name="." onClick={(e) => this.props.onClick(e.target.name)}>
          .
        </button>
        <button
          name={this.state.randomNumber[9]}
          onClick={(e) => this.shuffle(e)}
        >
          {this.state.randomNumber[9]}
        </button>
        <button name="=" onClick={(e) => this.props.onClick(e.target.name)}>
          =
        </button>
        <button name="/" onClick={(e) => this.props.onClick(e.target.name)}>
          ÷
        </button>
        <br />
      </div>
    );
  }
}

export default KeyPadComponent;
