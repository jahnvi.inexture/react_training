import React, { Component } from "react";
import Time from "react-time-format";

class News extends Component {
  newsItem;
  constructor(props) {
    super(props);
    this.newsItem = props.location.state.newsItem;
  }
  render() {
    return (
      <div>
        <div className="detail_card">
          <div>
            <h3 className="detail-title" style={{ textAlign: "center", fontWeight: "bolder", fontSize: "40px" }}>{this.newsItem.title}</h3>
          </div>
          <div className="container">
            <div className="row">
              <div className="card">
                <div className="card-body">
                  <img
                    src={this.newsItem.urlToImage}
                    class="card-img"
                    alt="news-image"
                    width="30"
                    height="400"
                  />
                  <h5 className="detail-card-author">
                    <strong>Author Name:</strong> {this.newsItem.author}
                  </h5>
                  <p className="card-text">
                    <strong>Title: </strong>
                    {this.newsItem.title}
                  </p>
                  <p className="card-text">
                    <strong>Description: </strong>
                    {this.newsItem.description}
                  </p>
                  <p className="card-text">
                    <strong>content: </strong>
                    {this.newsItem.content}
                  </p>
                  <p className="card-text">
                    <strong>Published At:</strong>{" "}
                    <Time
                      value={this.newsItem.publishedAt}
                      format="YYYY-MM-DD hh:mm:ss"
                    />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default News;
