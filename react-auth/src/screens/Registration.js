import { Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Link } from "react-router-dom";
import { useDispatch, connect } from "react-redux";
import { CallUserRegister } from "../reducer/actions";

import { RegistrationSchema } from "../validations/RegistrationSchema";

function Registration(props) {
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm({
    resolver: yupResolver(RegistrationSchema),
    reValidateMode: "onChange",
    mode: "all",
  });

  const onSubmit = (data) => {
    dispatch(CallUserRegister(data))
  };

  console.log("all fields ========>", watch());
  return (
    <>
      <div className="outer">
        <div className="inner">
          <form onSubmit={handleSubmit(onSubmit)}>
            <h3>Register</h3>
            <div className="form-group">
              <label id="r1">Name</label>
              <input
                type="text"
                className="form-control"
                placeholder="Enter Full Name"
                name="name"
                id="R1"
                {...register("name")}
              />
              <p class="styles">{errors.name?.message}</p>
            </div>

            <div className="form-group">
              <label id="r2">Email</label>
              <input
                type="email"
                className="form-control"
                placeholder="Enter email"
                id="R2"
                name="email"
                {...register("email")}
              />
              <p class="styles">{errors.email?.message}</p>
            </div>

            <div className="form-group">
              <label id="r3">Password</label>
              <input
                type="password"
                className="form-control"
                placeholder="Enter password"
                id="R3"
                name="password"
                {...register("password")}
              />
              <p class="styles">{errors.password?.message}</p>
            </div>

            <div className="form-group">
              <label id="r4">Phone No.</label>
              <input
                type="number"
                className="form-control"
                placeholder="Enter contact no"
                id="R4"
                name="number"
                {...register("number")}
              />
              <p class="styles">{errors.number?.message}</p>
            </div>

            <div className="form-group">
              <label id="r5">Choose your Profession</label>
              <Form.Control as="select" id="R5">
                <option>Developer</option>
                <option>Artist</option>
                <option>Photographer</option>
                <option>Team Player</option>
                <option>Full Stack</option>
              </Form.Control>
            </div>

            <button
              type="submit"
              className="btn btn-dark btn-lg btn-block"
              id="regbtn"
            >
              Register
            </button>
            <p className="forgot-password text-right">
              Already registered <Link to="/login">Login?</Link>
            </p>
          </form>
        </div>
      </div>
    </>
  );
}
const mapStateToProps = (state) => {
  return {
    user: state,
  };
};

const mapDispatchToProps = {
  CallUserRegister,
};

export default connect(mapStateToProps, mapDispatchToProps)(Registration);
