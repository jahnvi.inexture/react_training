import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import {LoginSchema} from '../validations/LoginSchema';
import { connect, useDispatch } from 'react-redux';
import { CallUserLogin } from '../reducer/actions';

function Login() {
  const dispatch = useDispatch();
  const [email, setEmail] = useState(" ");
  const [password, setPassword] = useState(" ");
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(LoginSchema),
    reValidateMode: "onChange",
    mode: "all",
  });

  const onSubmit = (data) => {
    dispatch(CallUserLogin(data));
  };
  const mailHandler = (e) => {
    setEmail(e.target.value);
    console.log("eeee", e);
  };
  const passHandler = (e) => {
    setPassword(e.target.value);
    console.log("eeeep", e);
  };
  // console.log("all fields ========>", watch());
  return (
    <div className="outer">
      <div className="inner">
        <form onSubmit={handleSubmit(onSubmit)}>
          <h3>LogIn</h3>
          <div className="form-group">
            <label id="l1">Email</label>
            <input
              type="email"
              id="L1"
              name="email"
              className="form-control"
              placeholder="Enter email"
              onChange={mailHandler}
              // onChange={(event) => setEmail(event.target.value)}
              {...register("email")}
            />
            <p class="styles">{errors?.email?.message}</p>
          </div>

          <div className="form-group">
            <label id="l2">Password</label>
            <input
              type="password"
              id="L2"
              name="password"
              className="form-control"
              placeholder="Enter password"
              onChange={passHandler}
              //onChange={(event) => setPassword(event.target.value)}
              {...register("password")}
            />
            <p class="styles">{errors?.password?.message}</p>
          </div>

          <button
            type="submit"
            className="btn btn-dark btn-lg btn-block"
            id="loginbtn"
          >
            Login
          </button>
        </form>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state,
  };
};

const mapDispatchToProps = {
  CallUserLogin,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
