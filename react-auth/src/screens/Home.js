import { connect, useDispatch } from "react-redux";
import { NavDropdown } from "react-bootstrap";
import NewsContext from "../components/NewsContext";
import { UserLogout } from "../reducer/actions";

const Home = () => {
  const dispatch = useDispatch();
  let userInfo = JSON.parse(localStorage.getItem("jahnvi"));
  let user = userInfo["name"];

  function logout() {
    dispatch(UserLogout());
  }

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container">
          <NavDropdown title={user}>
            <NavDropdown.Item onClick={logout}>Logout</NavDropdown.Item>
          </NavDropdown>
        </div>
      </nav>
      <h2
        style={{ textAlign: "center", fontWeight: "bolder", fontSize: "40px" }}
      >
        Top News
      </h2>
      <NewsContext />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state,
  };
};

const mapDispatchToProps = {
  UserLogout,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
