import React from "react";
import "./App.css";
import Registration from "./screens/Registration";
import { Router, Route, Switch, Link } from "react-router-dom";
import Login from "./screens/Login";
import Home from "./screens/Home";
import history from './reducer/history';
import News from './screens/News';

const App = () => {
  return (
    <>
      <Router history={history}>
        <div className="App">
          <Route>
            <ul>
              <li>
                <Link to="/"></Link>
              </li>
              <li>
                <Link to="/login"></Link>
              </li>
              <li>
                <Link to="/home"></Link>
              </li>
      
            </ul>
          </Route>
          <Switch>
            <Route exact path="/" component={Registration} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/home" component={Home} />
            <Route exact path='/News' component={News} /> 
          </Switch>
        </div>
      </Router>
    </>
  );
};

export default App;
