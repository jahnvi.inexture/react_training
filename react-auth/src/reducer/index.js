import {
  USER_REGISTER,
  USER_LOGIN,
  LOGOUT,
  GET_NEWS_ARTICLES,
  SET_NEWS_ARTICLES,
  SET_NEWS_ARTICLES_ON_ERROR,
} from "./types";

const initialState = {
  newUser: {},
  isUserRegister: false,
  isUserLogin: false,

  articles: [],
  fetchedArticlesOnError: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_REGISTER:
      return {
        ...state,
        newUser: action?.user,
      };
    case USER_LOGIN:
      return {
        ...state,
        newUser: {},
      };
    case LOGOUT:
      return {
        newUser: {},
        ...initialState,
      };
    case GET_NEWS_ARTICLES:
      return {
        ...state,
      };
    case SET_NEWS_ARTICLES:
      return {
        ...state,
        articles: action?.articlesList,
      };
    case SET_NEWS_ARTICLES_ON_ERROR:
      return {
        ...state,
        articles: [],
        fetchedArticlesOnError: action?.error,
      };
    default:
      return state;
  }
};
