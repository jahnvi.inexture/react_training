import {
  USER_REGISTER,
  SET_NEWS_ARTICLES,
  GET_NEWS_ARTICLES,
  SET_NEWS_ARTICLES_ON_ERROR,
  LOGOUT,
} from "./types";
import history from "./history";

export function setUserRegister(user) {
  console.log("setUserRegister");
  return {
    type: USER_REGISTER,
    user,
  };
}

export function CallUserRegister(data) {
  return (dispatch) => {
    dispatch(setUserRegister(data));
    localStorage.setItem("jahnvi", JSON.stringify(data));
    alert("You've Registered successfully");
  };
}

export function CallUserLogin(data) {
  let details = JSON.parse(localStorage.getItem("jahnvi"));
  if (
    data["email"] === details["email"] &&
    data["password"] === details["password"]
  ) {
    history.push("/home");
  } else {
    history.push("/");
    alert("Invalid email or password");
  }
}

export function UserLogout() {
  console.log("bye");
  history.goBack("/");
  return {
    type: LOGOUT,
  };
}

export function getNewsArticles() {
  console.log("getNewsArticles");
  return {
    type: GET_NEWS_ARTICLES,
  };
}
export function setNewsArticles(articles) {
  console.log("setNewsArticles");
  return {
    type: SET_NEWS_ARTICLES,
    articlesList: articles,
  };
}
export function setArticlesOnError(errors) {
  console.log("setArticlesOnError");
  return {
    type: SET_NEWS_ARTICLES_ON_ERROR,
    error: errors,
  };
}

export function fetchNewsArticles() {
  return (dispatch) => {
    dispatch(getNewsArticles());
    fetch(
      "https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=40898fe5842b4aa99bf9f9f712ef67bf"
    )
      .then((Response) => Response.json())
      .then((findresponse) => {
        console.log('findresponse', findresponse)
        dispatch(setNewsArticles(findresponse.articles));
      })
      .catch((err) => {
        console.log("error...",err);
        dispatch(setArticlesOnError(err));
      });
  };
}
