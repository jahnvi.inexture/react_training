import { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import "./NewsContext.css";
import Loader from "./Loader";
import Time from "react-time-format";
import { fetchNewsArticles } from "../reducer/actions";

class NewsContext extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articles: [],
      loading: false,
    };
  }
  componentDidMount() {
    const { fetchNewsArticles } = this.props;
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false });
    }, 2000);

    fetchNewsArticles();
  }
  render() {
    let data;
    if (this.state.loading) {
      data = <Loader />;
    } else {
      data = (
        <div className="all__news">
          {this.props.articles &&
            this.props.articles.length > 0 &&
            this.props.articles.slice(0, this.state.visible).map((articles) => (
              <div className="news">
                <div className="card" style={{ width: "18rem" }}>
                  <img
                    className="card-img-top"
                    src={articles.urlToImage}
                    alt="Card image cap"
                  />
                  <div className="card-body">
                    <h5 className="card-title">{articles.title}</h5>
                    <p className="card-text">{articles.author}</p>
                    <p className="card-text">
                      <small className="text-muted">
                        <Time
                          value={articles.publishedAt}
                          format="YYYY-MM-DD hh:mm:ss"
                        />
                      </small>
                    </p>
                    <Link
                      to={{
                        pathname: "/News",
                        state: {
                          newsItem: articles,
                        },
                      }}
                    >
                      <a href={articles.url} className="btn btn-primary">
                        Read more
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
            ))}
        </div>
      );
    }
    return <div>{data}</div>;
  }
}

const mapStateToProps = (state) => ({
  articles: state.articles,
});

const mapDispatchToProps = {
  fetchNewsArticles,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsContext);
