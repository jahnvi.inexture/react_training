import React from "react";
import { BeatLoader } from "react-spinners";

const Loader = () => {
  return <BeatLoader color="blue" size={72} />;
};

export default Loader;
