import React, { Component } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  NavLink,
  Redirect,
  Prompt,
} from "react-router-dom";
import Route from "react-router-dom/Route";

const User = (params) => {
  return <h1>Welcome user {params.username} </h1>;
};

class App extends Component {
  state = {
    loggedIn: false,
  };
  loginHandler = () => {
    this.setState((prevState) => ({
      loggedIn: !prevState.loggedIn,
    }));
  };
  render() {
    return (
      <Router>
        <div className="App">
          <ul>
            <li>
              <NavLink to="/" activeStyle={{ color: "green" }} exact>
                Home
              </NavLink>
            </li>
            <li>
              <NavLink to="/about" activeStyle={{ color: "green" }} exact>
                About
              </NavLink>
            </li>
            <li>
              <NavLink to="/user/Jahnvi" activeStyle={{ color: "green" }} exact>
                User1
              </NavLink>
            </li>
            <li>
              <NavLink to="/user/Nambi" activeStyle={{ color: "green" }} exact>
                User2
              </NavLink>
            </li>
          </ul>

          <Prompt
            when={!this.state.loggedIn}
            message={(location) => {
              return location.pathname.startsWith("/user")
                ? "Opps..!! You're not logged In"
                : true; // means don't prompt this message
            }}
          />

          <input
            type="button"
            value={this.state.loggedIn ? "log out" : "log in"}
            onClick={this.loginHandler.bind(this)}
          />

          <Route
            path="/"
            exact
            render={() => {
              return <h1>Introduction to Router</h1>;
            }}
          />
          <Route
            path="/about"
            exact
            strict
            render={() => {
              return <h1>About</h1>;
            }}
          />
          <Route
            path="/user/:username"
            exact
            strict
            render={({ match }) =>
              this.state.loggedIn ? (
                <User username={match.params.username} />
              ) : (
                <Redirect to="/" />
              )
            }
          />
        </div>
      </Router>
    );
  }
}

export default App;
